import React, { useState } from "react";

// import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import "./Expenses.css";
import ExpensesFilter from "../ExpensesFilter/ExpensesFilter";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "./ExpensesChart";


function Expenses(props) {
  const [selectedFilter, setSelectedFilter] = useState("2021");
  
  
  const filteredExpenses = props.items.filter((expense) => {
    return expense.date.getFullYear().toString() === selectedFilter;
  });
  
  const filterChangeHandler = (newFilter) => {
    setSelectedFilter(newFilter);
  };
  
  
  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          selected={selectedFilter}
          onChangeFilter={filterChangeHandler}
        />
        <ExpensesChart expenses={filteredExpenses} />
        <ExpensesList items={filteredExpenses}/>
      </Card>
    </div>
  );
}

export default Expenses;
